import java.util.Scanner;

public class Evaluado4 {
public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		int n = 0;
		System.out.println("Introduzca un n�mero entero negativo o superior a 100");
		n = s.nextInt();
		
		if(n > 100) {
			System.out.println("El n�mero es superior a 100");
		}else if(n < 0) {
			System.out.println("El n�mero es negativo");
		}else if(n == 100) {
			System.out.println("El n�mero es 100 exacto");
		}else {
			System.out.println("El n�mero est� fuera de los rangos especificados");
		}
		
	}
}

