import java.util.Scanner;

public class Evaluado2 {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int n = 0;
		System.out.println("Introduzca un n�mero para saber si es positivo negativo o nulo");
		n = s.nextInt();
		
		if(n == 0) {
			System.out.println("Es un valor nulo");
		}else if(n <= 0) {
			System.out.println("Es un valor negativo");
		}else {
			System.out.println("Es un valor positivo");
		}

	}
}
