import java.util.Scanner;

public class Evaluado3 {

	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		int n = 0;
		System.out.println("Introduzca un n�mero dentro del rango entre 51-100");
		n = s.nextInt();

		if(n > 50 && n < 101) {
			System.out.println("El n�mero est� dentro del rango");
		}else {
			System.out.println("El n�mero est� fuera del rango");
		}
		
	}

}
