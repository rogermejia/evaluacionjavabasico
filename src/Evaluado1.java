import java.util.Scanner;

public class Evaluado1 {
	
	

	public static void main(String[] args) {
		System.out.println("Introduzca el n�mero de la prueba a evaluar");
		
		
		Scanner z = new Scanner(System.in);
		int zu = z.nextInt();
		
		
		if(zu == 1) {
			//Prueba 1
			Scanner s = new Scanner(System.in);
			int n = 0;
			System.out.println("Introduzca un n�mero para saber si es par o impar");
			n = s.nextInt();
			
			if(n %2 == 0) {
				System.out.println("Es numero par");
			}else {
				System.out.println("Es numero impar");
			}
			
			
		}else if(zu == 2) {
			
			Scanner s = new Scanner(System.in);
			int n = 0;
			System.out.println("Introduzca un n�mero para saber si es positivo negativo o nulo");
			n = s.nextInt();
			
			if(n == 0) {
				System.out.println("Es un valor nulo");
			}else if(n <= 0) {
				System.out.println("Es un valor negativo");
			}else {
				System.out.println("Es un valor positivo");
			}
			
			
		}else if(zu == 3) {
			
			Scanner s = new Scanner(System.in);
			int n = 0;
			System.out.println("Introduzca un n�mero dentro del rango entre 51-100");
			n = s.nextInt();

			if(n > 50 && n < 101) {
				System.out.println("El n�mero est� dentro del rango");
			}else {
				System.out.println("El n�mero est� fuera del rango");
			}
			
		}else if(zu == 4) {
			
			Scanner s = new Scanner(System.in);
			int n = 0;
			System.out.println("Introduzca un n�mero entero negativo o superior a 100");
			n = s.nextInt();
			
			if(n > 100) {
				System.out.println("El n�mero es superior a 100");
			}else if(n < 0) {
				System.out.println("El n�mero es negativo");
			}else if(n == 100) {
				System.out.println("El n�mero es 100 exacto");
			}else {
				System.out.println("El n�mero est� fuera de los rangos especificados");
			}
			
		}else if(zu == 5) {
			
			Scanner s = new Scanner(System.in);
			Double edades = 0.0;
			int n = 1;
			int i = 0;
			System.out.println("Introduzca las edades que desea promediar");
			//s.nextInt();
			
			while(n!=0) {
				n= s.nextInt();
				System.out.println("presione 0 cuando desee finalizar");
				
				edades= edades + n;
		
				i++;
			}
			
			//System.out.println("Edades " + edades);
			Double promedio = edades;
			
			 i--;
			
			promedio = edades/i;
			System.out.println("El promedio de edades es de: "+promedio);

		}else if(zu == 6) {
			
			int[] a = new int[20];
			
			for(int i = 0; i<20; i++) {
				int n = 0;
				
				do{
					n =  (int) Math.round(80* Math.random());
				}while(n<=20 || n>=80 && n==0);
				 
				//if(n>=20 && n<=80 && n!=0) {
				 a[i] = n;
				 //}
				
			}
			
			//n>=20 && n<=80 && n!=0
			Integer j = 0;
			for (int i = 0; i < a.length; i++) {
				j++;
				System.out.println("Numero de la posici�n "+j +" "+a[i]);
			}
		
			
		}else if(zu == 7) {
			
			int i = 0;
			Double area = 0.0;
			Double a = 0.0;
			Double h=0.0;
			Double b=0.0;
			Double r=0.0;
			Scanner s = new Scanner(System.in);
			System.out.println("Presione para calcular el area de: ");
			System.out.println("1. Cuadrado ");
			System.out.println("2. Rect�ngulo ");
			System.out.println("3. Tri�ngulo ");
			System.out.println("4. C�rculo ");
			i = s.nextInt();
			switch (i) {
			case 1:
				System.out.println("Ingrese el tama�o del lado del cuadrado ");
				a = s.nextDouble();
				area = Math.pow(a, 2);
				System.out.println("Area total = " + area);
				break;

			case 2:
				System.out.println("Ingrese el tama�o de la base del rect�ngulo ");
				b = s.nextDouble();
				
				System.out.println("Ingrese el tama�o de la altura del rect�ngulo ");
				h = s.nextDouble();
				area = b*h;
				System.out.println("Area total = " + area);
				
				break;
			case 3:
				System.out.println("Ingrese el tama�o de la base del tri�ngulo ");
				b = s.nextDouble();
				
				System.out.println("Ingrese el tama�o de la altura del tri�ngulo ");
				h = s.nextDouble();
				
				area = (b*h)/2;
				
				
				System.out.println("Area total = " + area);
				
				
				break;
			case 4:
				System.out.println("Ingrese el tama�o del r�dio del c�rculo ");
				r = s.nextDouble();
				
				area = Math.PI * (Math.pow(r, 2));
				
				System.out.println("Area total = " + area);
				break;
			}
			
		}else if(zu == 8) {
			
			int[][] mat = new int[9][9];
			int n = 9;
			for(int i = 0; i<=8; i++) {
				for (int j = 0; j <= 8; j++) {
					
					if(n<= 1) {
						mat[i][j]=1;
						n = 8;
					}else {
						
						mat[j][i] = n--;
					}
					
				}
			}
			
			

			for (int i = 0; i <= 8; i++) {
				
				for (int j = 0; j <= 8; j++) {
					System.out.print(mat[i][j] + "");
				}
				System.out.println(" ");
			}
		}
			
		}
     
		

	

}
